import dict_tools.differ as differ
import pytest


RESOURCE_NAME = "my-resource"
SCOPE = "/subscriptions/{subscriptionId}/resourceGroups/my-resource-group"
POLICY_ASSIGNMENT_NAME = "my-resource-group"
RESOURCE_PARAMETERS = {
    "properties": {
        "displayName": "Enforce resource naming rules",
        "description": "Force resource names to begin with given DeptA and end with -LC",
        "metadata": {"assignedBy": "Special Someone"},
        "parameters": {"prefix": {"value": "DeptA"}, "suffix": {"value": "-LC"}},
        "nonComplianceMessages": [
            {"message": "Resource names must start with 'DeptA' and end with '-LC'."}
        ],
    }
}


@pytest.mark.skip(
    reason="Test will be disabled until module is refactored with new design changes to support caching"
)
@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of policy assignments. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.policy.policy_assignments.present = (
        hub.states.azure.policy.policy_assignments.present
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/my-resource-group"
            f"/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Would create azure.policy.policy_assignments",
    }

    scope = SCOPE.replace("{subscriptionId}", ctx.acct.subscription_id)

    def _check_get_parameters(_ctx, url, success_codes):
        assert POLICY_ASSIGNMENT_NAME in url
        assert scope in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert POLICY_ASSIGNMENT_NAME in url
        assert scope in url
        assert json == RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.policy.policy_assignments.present(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
        RESOURCE_PARAMETERS,
    )
    assert differ.deep_diff(dict(), expected_put.get("ret")) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_put.get("comment") == ret.get("comment")


@pytest.mark.skip(
    reason="Test will be disabled until module is refactored with new design changes to support caching"
)
@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of policy assignments. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.policy.policy_assignments.present = (
        hub.states.azure.policy.policy_assignments.present
    )
    patch_parameter = {"identity": {"type": "SystemAssigned"}}

    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/my-resource-group"
            f"/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_patch = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/my-resource-group"
            f"/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
            **patch_parameter,
        },
        "result": True,
        "status": 200,
        "comment": f"Would update azure.policy.policy_assignments with parameters: {patch_parameter}.",
    }

    scope = SCOPE.replace("{subscriptionId}", ctx.acct.subscription_id)

    def _check_patch_parameters(_ctx, url, success_codes, json):
        assert POLICY_ASSIGNMENT_NAME in url
        assert scope in url
        assert json == patch_parameter
        return expected_patch

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.json.patch.side_effect = _check_patch_parameters
    mock_hub.tool.azure.request.patch_json_content.return_value = patch_parameter

    ret = await mock_hub.states.azure.policy.policy_assignments.present(
        ctx, RESOURCE_NAME, SCOPE, POLICY_ASSIGNMENT_NAME, patch_parameter
    )
    assert differ.deep_diff(
        expected_get.get("ret"), expected_patch.get("ret")
    ) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_patch.get("comment") == ret.get("comment")


@pytest.mark.skip(
    reason="Test will be disabled until module is refactored with new design changes to support caching"
)
@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of policy assignments. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.policy.policy_assignments.absent = (
        hub.states.azure.policy.policy_assignments.absent
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    scope = SCOPE.replace("{subscriptionId}", ctx.acct.subscription_id)

    def _check_get_parameters(_ctx, url, success_codes):
        assert POLICY_ASSIGNMENT_NAME in url
        assert scope in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.policy.policy_assignments.absent(
        ctx,
        RESOURCE_NAME,
        SCOPE,
        POLICY_ASSIGNMENT_NAME,
    )
    assert not ret["changes"]
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert f"'{RESOURCE_NAME}' already absent" == ret.get("comment")


@pytest.mark.skip(
    reason="Test will be disabled until module is refactored with new design changes to support caching"
)
@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of policy assignments. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.policy.policy_assignments.absent = (
        hub.states.azure.policy.policy_assignments.absent
    )
    expected_get = {
        "ret": {
            "id": f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/my-resource-group"
            f"/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS,
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    scope = SCOPE.replace("{subscriptionId}", ctx.acct.subscription_id)

    def _check_delete_parameters(_ctx, url, success_codes):
        assert POLICY_ASSIGNMENT_NAME in url
        assert scope in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.policy.policy_assignments.absent(
        ctx,
        RESOURCE_NAME,
        scope,
        POLICY_ASSIGNMENT_NAME,
    )
    assert differ.deep_diff(expected_get.get("ret"), dict()) == ret.get("changes")
    assert RESOURCE_NAME == ret.get("name")
    assert ret["result"]
    assert expected_delete.get("comment") == ret.get("comment")


@pytest.mark.skip(
    reason="Test will be disabled until module is refactored with new design changes to support caching"
)
@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of policy assignments.
    """
    mock_hub.states.azure.policy.policy_assignments.describe = (
        hub.states.azure.policy.policy_assignments.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value = hub.tool.azure.uri.get_parameter_value
    resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/my-resource-group"
        f"/providers/Microsoft.Authorization/policyAssignments/{POLICY_ASSIGNMENT_NAME}"
    )
    scope = SCOPE.replace("{subscriptionId}", ctx.acct.subscription_id)
    RESOURCE_PARAMETERS.get("properties").update({"scope": scope})
    expected_list = {
        "ret": {
            "value": [{"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS}]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.policy.policy_assignments.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.policy.policy_assignments.present" in ret_value.keys()
    expected_describe = [
        {"scope": scope},
        {"policy_assignment_name": POLICY_ASSIGNMENT_NAME},
        {"parameters": expected_list.get("ret").get("value")[0]},
    ]
    assert expected_describe == ret_value.get("azure.policy.policy_assignments.present")
