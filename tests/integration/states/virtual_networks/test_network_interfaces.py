import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_network_interface(hub, ctx, resource_group_fixture, subnet_fixture):
    """
    This test provisions a network interface, describes network interface and deletes network interface.
    """
    # Create network interface
    resource_group_name = resource_group_fixture.get("name")
    network_interface_name = "idem-test-network_interface-" + str(int(time.time()))
    subnet_id = subnet_fixture.get("id")
    nic_parameters = {
        "location": "eastus",
        "tags": {
            f"idem-test-tag-key-"
            + str(int(time.time())): f"idem-test-tag-value-"
            + str(int(time.time()))
        },
        "ip_configurations": [
            {
                "name": "test-ipc",
                "private_ip_address_allocation": "Static",
                "subnet_id": subnet_id,
                "private_ip_address_version": "IPv4",
                "private_ip_address": "10.0.0.12",
                "primary": True,
            }
        ],
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create network interface with --test
    nic_ret = await hub.states.azure.virtual_networks.network_interfaces.present(
        test_ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        **nic_parameters,
    )
    assert nic_ret["result"], nic_ret["comment"]
    assert not nic_ret["old_state"] and nic_ret["new_state"]
    assert (
        f"Would create azure.virtual_networks.network_interfaces '{network_interface_name}'"
        in nic_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=nic_ret["new_state"],
        expected_old_state=None,
        expected_new_state=nic_parameters,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        idem_resource_name=network_interface_name,
    )
    resource_id = nic_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Network/networkInterfaces/{network_interface_name}"
        == resource_id
    )

    # Create network interface in real
    nic_ret = await hub.states.azure.virtual_networks.network_interfaces.present(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        **nic_parameters,
    )
    assert nic_ret["result"], nic_ret["comment"]
    assert not nic_ret["old_state"] and nic_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=nic_ret["new_state"],
        expected_old_state=None,
        expected_new_state=nic_parameters,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        idem_resource_name=network_interface_name,
    )
    resource_id = nic_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Network/networkInterfaces/{network_interface_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-08-01",
        retry_count=5,
        retry_period=10,
    )

    # Describe network interface
    describe_ret = await hub.states.azure.virtual_networks.network_interfaces.describe(
        ctx
    )
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.virtual_networks.network_interfaces.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        idem_resource_name=resource_id,
    )

    nic_update_parameters = {
        "location": "eastus",
        "tags": {
            f"idem-test-tag-key-"
            + str(int(time.time())): f"idem-test-tag-value-"
            + str(int(time.time()))
        },
        "ip_configurations": [
            {
                "name": "test-ipc-updated",
                "private_ip_address_allocation": "Static",
                "subnet_id": subnet_id,
                "private_ip_address_version": "IPv4",
                "private_ip_address": "10.0.0.13",
                "primary": True,
            }
        ],
    }
    # Update policy definition with --test
    nic_ret = await hub.states.azure.virtual_networks.network_interfaces.present(
        test_ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        **nic_update_parameters,
    )
    assert nic_ret["result"], nic_ret["comment"]
    assert nic_ret["old_state"] and nic_ret["new_state"]
    assert (
        f"Would update azure.virtual_networks.network_interfaces '{network_interface_name}'"
        in nic_ret["comment"]
    )
    check_returned_states(
        old_state=nic_ret["old_state"],
        new_state=nic_ret["new_state"],
        expected_old_state=nic_parameters,
        expected_new_state=nic_update_parameters,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        idem_resource_name=network_interface_name,
    )
    resource_id = nic_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Network/networkInterfaces/{network_interface_name}"
        == resource_id
    )

    # Update policy definition in real
    nic_ret = await hub.states.azure.virtual_networks.network_interfaces.present(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        **nic_update_parameters,
    )
    assert nic_ret["result"], nic_ret["comment"]
    assert nic_ret["old_state"] and nic_ret["new_state"]
    assert (
        f"Updated azure.virtual_networks.network_interfaces '{network_interface_name}'"
        in nic_ret["comment"]
    )
    check_returned_states(
        old_state=nic_ret["old_state"],
        new_state=nic_ret["new_state"],
        expected_old_state=nic_parameters,
        expected_new_state=nic_update_parameters,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        idem_resource_name=network_interface_name,
    )
    resource_id = nic_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Network/networkInterfaces/{network_interface_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-08-01",
        retry_count=5,
        retry_period=10,
    )

    # Delete network interfaces with --test
    nic_del_ret = await hub.states.azure.virtual_networks.network_interfaces.absent(
        test_ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
    )
    assert nic_del_ret["result"], nic_del_ret["comment"]
    assert nic_del_ret["old_state"] and not nic_del_ret["new_state"]
    assert (
        f"Would delete azure.virtual_networks.network_interfaces '{network_interface_name}'"
        in nic_del_ret["comment"]
    )
    check_returned_states(
        old_state=nic_del_ret["old_state"],
        new_state=None,
        expected_old_state=nic_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        idem_resource_name=network_interface_name,
    )

    # Delete network interfaces in real
    nic_del_ret = await hub.states.azure.virtual_networks.network_interfaces.absent(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
    )
    assert nic_del_ret["result"], nic_del_ret["comment"]
    assert nic_del_ret["old_state"] and not nic_del_ret["new_state"]
    assert (
        f"Deleted azure.virtual_networks.network_interfaces '{network_interface_name}'"
        in nic_del_ret["comment"]
    )
    check_returned_states(
        old_state=nic_del_ret["old_state"],
        new_state=None,
        expected_old_state=nic_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
        idem_resource_name=network_interface_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-08-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete network interface again
    nic_del_ret = await hub.states.azure.virtual_networks.network_interfaces.absent(
        ctx,
        name=network_interface_name,
        resource_group_name=resource_group_name,
        network_interface_name=network_interface_name,
    )
    assert nic_del_ret["result"], nic_del_ret["comment"]
    assert not nic_del_ret["old_state"] and not nic_del_ret["new_state"]
    assert (
        f"azure.virtual_networks.network_interfaces '{network_interface_name}' already absent"
        in nic_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    network_interface_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert network_interface_name == old_state.get("network_interface_name")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["tags"] == old_state.get("tags")
        assert expected_old_state["ip_configurations"] == old_state.get(
            "ip_configurations"
        )
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert network_interface_name == new_state.get("network_interface_name")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["tags"] == new_state.get("tags")
        assert expected_new_state["ip_configurations"] == new_state.get(
            "ip_configurations"
        )
